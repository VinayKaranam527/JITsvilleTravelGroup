package com.travelgroup.JITSvilleTravelGroup;

public class Passenger {
	
	private int passengerId;
	private double rateFactor =0.5;
	private int stopsOrMiles;
	private String typeOfPassenger;
	private int NewspaperRequired;
	
	public int getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(int passengerId) {
		this.passengerId = passengerId;
	}
	public Passenger(int passengerId,int stopsOrMiles,String typeOfPassenger,int NewspaperRequired)
	{
		this.passengerId = passengerId;
		this.stopsOrMiles = stopsOrMiles;
		this.typeOfPassenger = typeOfPassenger;
		this.NewspaperRequired = NewspaperRequired;
	}
	
	public double getRateFactor() {
		return rateFactor;
	}
	public void setRateFactor(double rateFactor) {
		this.rateFactor = rateFactor;
	}
	public int getStopsOrMiles() {
		return stopsOrMiles;
	}
	public void setStopsOrMiles(int stopsOrMiles) {
		this.stopsOrMiles = stopsOrMiles;
	}
	public String getTypeOfPassenger() {
		return typeOfPassenger;
	}
	public void setTypeOfPassengers(String typeOfPassenger) {
		this.typeOfPassenger = typeOfPassenger;
	}
	
	public int isNewspaperRequired() {
		return NewspaperRequired;
	}
	public void setNewspaperRequired(int newspaperRequired) {
		NewspaperRequired = newspaperRequired;
	}
	

}
