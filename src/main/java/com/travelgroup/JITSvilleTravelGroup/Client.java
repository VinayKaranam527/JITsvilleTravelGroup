package com.travelgroup.JITSvilleTravelGroup;

import java.util.ArrayList;
import java.util.List;

class Client {
    public static void main(String[] args)
    	{
    List<Passenger> passengerlist = new ArrayList<Passenger>();
    passengerlist.add(new Passenger(1,3,"Frequent Rider",0));
	passengerlist.add(new Passenger(2,5,"Frequent Rider",0));
	passengerlist.add(new Passenger(3,4,"Commuter",1));
	passengerlist.add(new Passenger(4,90,"Vacationer",0));
	passengerlist.add(new Passenger(5,199,"Vacationer",1));
	
	PassengerFactory passFactory = new PassengerFactory();
	MealsFactory mealsFactory = new MealsFactory();
	try {
		double fare=0.0;
		double TotalFare=0.0;
		int numOfMeals=0;
		int TotalMealsCount =0;
		int countofNewspapers =0;
		int TotalNewspapersCount= 0;
		System.out.println("PassengerId\t" + "Fare\t\t"+ "NewsPapersOrdered\t"+ "Meals Ordered");
		System.out.println("-----------\t--------\t-----------------\t-----------");
	for(Passenger passenger: passengerlist)
	{
		if(passenger.getTypeOfPassenger().equalsIgnoreCase("Commuter"))
     {
		System.out.print(passenger.getPassengerId()+"\t\t");
		System.out.print(passFactory.getPassenger("Commuter").cost(passenger)+"\t\t");
        fare =passFactory.getPassenger("Commuter").cost(passenger);
        countofNewspapers = passFactory.getPassenger("Commuter").countofNewspapers(passenger);
        System.out.print(countofNewspapers+"\t\t\t");
        System.out.println(numOfMeals);
        TotalFare+=fare;
        TotalNewspapersCount+=countofNewspapers;
        
       }
		else if(passenger.getTypeOfPassenger().equalsIgnoreCase("Frequent Rider"))
		{
			System.out.print(passenger.getPassengerId()+ "\t\t");	
			System.out.print(passFactory.getPassenger("Frequent Rider").cost(passenger)+"\t\t");
	        fare = passFactory.getPassenger("Frequent Rider").cost(passenger);
	        countofNewspapers = passFactory.getPassenger("Frequent Rider").countofNewspapers(passenger);
	        System.out.print(countofNewspapers+"\t\t\t");
	        System.out.println(numOfMeals);
	        TotalFare+=fare;
	        TotalNewspapersCount+=countofNewspapers;
		}
		else if (passenger.getTypeOfPassenger().equalsIgnoreCase("Vacationer"))
		{
			System.out.print(passenger.getPassengerId()+ "\t\t");	
			System.out.print(passFactory.getPassenger("Vacationer").cost(passenger)+"\t\t");
	        fare = passFactory.getPassenger("Vacationer").cost(passenger);
	        numOfMeals = mealsFactory.getMealsCount("Vacationer").mealsCount(passenger);
	        countofNewspapers = passFactory.getPassenger("Vacationer").countofNewspapers(passenger);
            System.out.print(countofNewspapers+"\t\t\t");
	        System.out.println(numOfMeals);
	       	TotalFare+=fare;
	        TotalNewspapersCount+=countofNewspapers;
	        TotalMealsCount+=numOfMeals;
		}
	}
	System.out.println("\nTotal Fare:\t" +TotalFare);
	System.out.println("Total count of Newspapers:\t" +TotalNewspapersCount);
	System.out.println("Total meals ordered:\t" + TotalMealsCount);
	    }
	catch(IllegalArgumentException ex)
	{
		ex.printStackTrace();
	}
}
}
