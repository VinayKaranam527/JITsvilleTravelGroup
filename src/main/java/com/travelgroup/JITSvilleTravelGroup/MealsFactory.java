package com.travelgroup.JITSvilleTravelGroup;

public class MealsFactory {
	
	public CalculateMeals getMealsCount(String typeOfPassenger)
	{
		if(typeOfPassenger.equalsIgnoreCase("Vacationer") )
				{
		   return new  VacationPassenger();  	   
		}
		return null;
	}

}

