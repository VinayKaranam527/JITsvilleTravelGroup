package com.travelgroup.JITSvilleTravelGroup;

public class CommuterPassenger implements PassengerType {
	
	public double cost(Passenger passenger) {			
		// TODO Auto-generated method stub
		double fare =0;
		if(passenger.getTypeOfPassenger().equalsIgnoreCase("Commuter"))
		{
		fare = passenger.getRateFactor() * passenger.getStopsOrMiles();
		}
		else if(passenger.getTypeOfPassenger().equalsIgnoreCase("Frequent Rider"))
		{
			double cost =passenger.getRateFactor() * passenger.getStopsOrMiles();
			fare = cost - (0.1 * cost);
		}
		return fare;
	}

	public int countofNewspapers(Passenger passenger) {
		// TODO Auto-generated method stub
		int count=0;
		if(passenger.isNewspaperRequired() == 1)
		{
		 count++;
		}
		return count;
	}

}
