package com.travelgroup.JITSvilleTravelGroup;

public class VacationPassenger implements PassengerType, CalculateMeals {
	
	public double cost(Passenger passenger) {
		// TODO Auto-generated method stub
		double fare =0;
		if(passenger.getStopsOrMiles() < 5)
		{
			throw new IllegalArgumentException("Invalid Miles: less than 5 miles");
		}
		else if(passenger.getStopsOrMiles() > 4000)
		{
			throw new IllegalArgumentException("Invalid miles: more than 6000");
		}
		else
		{
			fare = Math.ceil(passenger.getRateFactor() * passenger.getStopsOrMiles() *100)/100;
		   
		}	
		 return fare;
	}

	public int countofNewspapers(Passenger passenger) {
		// TODO Auto-generated method stub
		int count=0;
		if(passenger.isNewspaperRequired() == 1)
		{
		 count++;
		}
		return count;
	}

	public int mealsCount(Passenger passenger)
	{
		int countmeals =0;
		if(passenger.getStopsOrMiles() < 5 || passenger.getStopsOrMiles() > 4000)
		{
			throw new IllegalArgumentException();
		}
		else
		{
		  countmeals = (passenger.getStopsOrMiles() / 100 ) + 1;
		}
		return countmeals;
	}
}
