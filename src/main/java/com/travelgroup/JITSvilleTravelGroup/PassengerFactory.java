package com.travelgroup.JITSvilleTravelGroup;

public class PassengerFactory {
	
	public PassengerType getPassenger(String typeOfPassenger)
	{
		if(typeOfPassenger.equalsIgnoreCase("Commuter") || typeOfPassenger.equalsIgnoreCase("Frequent Rider"))
		{
		   return new CommuterPassenger();  	   
		}
		else if(typeOfPassenger.equalsIgnoreCase("Vacationer"))
		{
			return new VacationPassenger();
		}
	
		return null;
	}
	

}
