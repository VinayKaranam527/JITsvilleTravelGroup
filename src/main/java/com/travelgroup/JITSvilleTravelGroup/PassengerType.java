package com.travelgroup.JITSvilleTravelGroup;

public interface PassengerType {
	
	double cost(Passenger passenger);
	int countofNewspapers(Passenger passenger);
    
}
