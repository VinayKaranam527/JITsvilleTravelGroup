package com.travelgroup.JITSvilleTravelGroup;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VacationPassengerTest {
	
	 VacationPassenger vacPass;
		
	@Before
	public void setUp()	
	{
	 vacPass = new VacationPassenger();
	}	
    @Rule
    public ExpectedException thrown = ExpectedException.none();
	@Test
	public void testCostVacationer1() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid Miles: less than 5 miles");
		vacPass.cost(new Passenger(1,3,"Vacationer",0));
	}
    @Test
    public void testCostVacationer2() {
		
    	assertEquals(12.5,vacPass.cost(new Passenger(2,25,"Vacationer",1)) , 0.01);
	}
    @Test
	public void testCostVacationer3() {
		
		assertEquals(225.0,vacPass.cost(new Passenger(3,450,"Vacationer",0)) , 0.01);
	}
   
	@Test
	public void testCostVacationer4() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid miles: more than 6000");
		assertEquals(1.35,vacPass.cost(new Passenger(4,6000,"Vacationer",0)) , 0.01);
	}
	
	@Test
	public void testCountofNewspapers1() {
		assertEquals(0,vacPass.countofNewspapers(new Passenger(4,90,"Vacationer",0)) , 0.01);
	}
	
	@Test
	public void testCountofNewspapers2() {
		assertEquals(1,vacPass.countofNewspapers(new Passenger(5,199,"Vacationer",1)) , 0.01);
	}
	
	@Test
	public void testMealsCount1() {
		assertEquals(1,vacPass.mealsCount(new Passenger(1,7,"Vacationer",0)) , 0.01);
	}
	@Test
	public void testMealsCount2() {
		assertEquals(4,vacPass.mealsCount(new Passenger(2,300,"Vacationer",0)) , 0.01);
	}

}
