package com.travelgroup.JITSvilleTravelGroup;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;



public class CommuterPassengerTest {


    CommuterPassenger comPass;
	
	@Before
	public void setUp()	
	{
		comPass = new CommuterPassenger();
	}
	
	
	@Test
	public void testCostOfFrequentRider() {
		
		assertEquals(1.35,comPass.cost(new Passenger(1,3,"Frequent Rider",0)) , 0.01);
	}

	@Test
	public void testCostOfCommuter() {
		
		assertEquals(2.0,comPass.cost(new Passenger(3,4,"Commuter",1)) , 0.01);
	}
	@Test
	public void testCountofNewspapers1() {
		assertEquals(1,comPass.countofNewspapers(new Passenger(3,4,"Commuter",1)) , 0.01);
	}
	@Test
	public void testCountofNewspapers2() {
		assertEquals(0,comPass.countofNewspapers(new Passenger(2,5,"Frequent Rider",0)) , 0.01);
	}


}
